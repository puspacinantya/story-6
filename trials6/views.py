import json
import webbrowser

import requests
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.db import IntegrityError
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.core import serializers
from django.contrib.auth import logout as auth_logout
from django.views.decorators.csrf import csrf_exempt

from .forms import Registforms, Statusforms
from .models import Registmodels, Statusmodels

# Create your views here.

response = {}
response['counter'] = 0

def index(request):
    response['savestatus'] = Statusforms
    table = Statusmodels.objects.all()
    response['table'] = table
    html = 'index.html'


    if(request.method == 'POST'):
        response['from_who'] = request.POST['from_who']
        response['statuses'] = request.POST['statuses']
        writestat = Statusmodels(from_who=response['from_who'],statuses=response['statuses'])
        writestat.save()
        html = 'index.html'
        return render(request, html, response)
    else:
        return render(request, html, response)

def profile(request):
    return render(request, 'profile.html', response)

def data(request, ganti):
    if(ganti == 'architecture'):
        json_file = requests.get('https://www.googleapis.com/books/v1/volumes?q=architecture').json()
    if(ganti == 'comic'):
        json_file = requests.get('https://www.googleapis.com/books/v1/volumes?q=comic').json()
    if(ganti == 'quilting'):
        json_file = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    return JsonResponse(json_file)

def books(request): 
    if request.user.is_authenticated:
        request.session['username'] = request.user.username
        request.session['email'] = request.user.email
        if 'counter' not in request.session:
            request.session['counter'] = 0
        response['counter'] = request.session['counter']
    else:
        if 'counter' not in request.session:
            request.session['counter'] = 0
        response['counter'] = request.session['counter']
    return render(request, 'books.html', response)

@csrf_exempt
def addcounter(request):
    if request.user.is_authenticated:
        request.session['counter'] = request.session['counter'] + 1
    else:
        pass
    return HttpResponse(request.session['counter'], content_type = 'application/json')

@csrf_exempt
def substractcounter(request):
    if request.user.is_authenticated:
        request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'], content_type = 'application/json')

def subscribe(request): 
    form = Registforms(request.POST)
    html = 'subscribe.html'
    return render(request,html,{'form':form})

def validate(request):
    email = request.POST['email']
    data = Registmodels.objects.filter(email=email)
    if data.exists():
        # print("ada")
        return JsonResponse({'is_exist': True})
    else:
        return JsonResponse({'is_exist': False})
    

def registviews(request):
    response['form']=Registforms()
    form=Registforms(request.POST or None)
    # code that produces error
    if request.method == 'POST'and form.is_valid():
        response['Nama'] = request.POST['name']
        response['Email'] = request.POST['email']
        response['Password'] = request.POST['password']
        status = 'valid'
        try:
            writereg = Registmodels(name=response['Nama'],email=response['Email'], password=response['Password'])
            writereg.save()
            return JsonResponse({'stat': status})

        except IntegrityError as e:
            status = 'invalid'
            return JsonResponse({'stat': status})
    else:
        return render(request, 'subscribe.html', response)

def userdata(request):
    all_obj = Registmodels.objects.all().values()
    user_list = list(all_obj)
    return JsonResponse(user_list, safe= False)

def deleteuser(request):
    email = request.GET['email']    
    Registmodels.objects.get(email=email).delete()
    return JsonResponse({})

def logout(request):
    request.session.flush()
    auth_logout(request)
    return HttpResponseRedirect('/trials6/profile/')