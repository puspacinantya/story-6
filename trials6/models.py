from django.db import models
from django.utils import timezone

#Create your models here.

class Statusmodels(models.Model):
    from_who = models.TextField(max_length=300)
    statuses = models.TextField(max_length=300)
    date_filled = models.DateField(default=timezone.now)

class Registmodels(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(unique=True,db_index=True)
    password = models.TextField(max_length=15)

