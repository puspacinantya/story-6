from django.conf.urls import url
from .views import index, profile, books, data, registviews, subscribe, validate, userdata, deleteuser, logout, addcounter, substractcounter


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profile/', profile, name='profile'),
    url(r'^books/', books, name='books'),
    url(r'^addcounter/', addcounter, name='addcounter'),
    url(r'^substractcounter/', substractcounter, name='substractcounter'),
    url(r'^subscribe/', subscribe, name='subscribe'),
    url(r'^registviews/', registviews, name='registviews'),
    url(r'^validate/', validate, name='validate'),
    url(r'^userdata/', userdata, name='userdata'),
    url(r'^deleteuser/', deleteuser, name='deleteuser'),
    url(r'^logout/', logout, name='logout'),
    url(r'^data/(?P<ganti>[\w\-]+)/$', data, name='data'),
]