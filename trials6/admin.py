from django.contrib import admin
from .models import Statusmodels, Registmodels

# Register your models here.
admin.site.register(Statusmodels)
admin.site.register(Registmodels)