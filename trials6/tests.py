from __future__ import unicode_literals
from django.utils import six
from django.utils.encoding import force_text
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver 
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options 
from django.db import IntegrityError
from selenium.webdriver.support.color import Color
from .views import index, profile, books, registviews, subscribe, validate
from .models import Statusmodels, Registmodels
from .forms import Statusforms, Registforms
import time


# Create your tests here.

# class Story6FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         super(Story6FunctionalTest, self).setUp()
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

#     def tearDown(self):
#         self.selenium.quit()
#         super(Story6FunctionalTest, self).tearDown()

#     def test_input_status(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         time.sleep(5)

#         # selenium.get('http://localhost:8000/trials6/')
#         # selenium.get(self.live_server_url + '/trials6')
#         selenium.get('http://story-6-puspa.herokuapp.com/trials6/')

#         time.sleep(5)
#         # find the form element
#         form = selenium.find_element_by_class_name('form-group')
#         user = selenium.find_element_by_name('from_who')
#         stat = selenium.find_element_by_name('statuses')
#         table = selenium.find_element_by_class_name('table')

#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         user.send_keys('functest')
#         stat.send_keys('Coba Coba')
#         time.sleep(5)

#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#         time.sleep(5)
#         self.assertIn('Coba Coba', self.selenium.page_source)

#     def test_story_6_layout(self):
#         selenium = self.selenium
#         time.sleep(5)

#         # selenium.get('http://localhost:8000/trials6/')
#         # selenium.get(self.live_server_url + '/trials6')
#         selenium.get('http://story-6-puspa.herokuapp.com/trials6/')
        
#         #testing if the h1 tag has the name of Hello Apa kabar
#         tag = selenium.find_element_by_tag_name('h1').text
#         self.assertIn("Hello, Apa kabar?", tag)

#         #testing if the attribute value has the "Submit"
#         sub = selenium.find_element_by_id('submit').get_attribute("value")
#         self.assertIn("Submit", sub)

#         time.sleep(5)

#     def test_story_6_css(self):
#         selenium = self.selenium
#         time.sleep(5)

#         # selenium.get('http://localhost:8000/trials6/')
#         # selenium.get(self.live_server_url + '/trials6')
#         selenium.get('http://story-6-puspa.herokuapp.com/trials6/profile/')

#         #testing if the css in .company has color of #d72c16
#         comp = selenium.find_element_by_css_selector('.company').value_of_css_property('color')
#         warna = Color.from_string(comp).hex
#         self.assertEqual('#d72c16', warna)

#         #testing if the css in comptext has text alignment of center
#         comptext = selenium.find_element_by_css_selector('.company').value_of_css_property('text-align')
#         self.assertEqual(comptext, 'center')

#         time.sleep(5)

class Story6UnitTest (TestCase):
    
    #checking if the link is directing to trials6
    def test_story_6_url_is_exist(self):
        response = Client().get('/trials6/')
        self.assertEqual(response.status_code, 200)

    #checking if the link uses my index function on views.py
    def test_story_6_using_index_and_profile_func(self):
        found = resolve('/trials6/')
        found2 = resolve('/trials6/profile/')
        found3 = resolve('/trials6/books/')
        found4 = resolve('/trials6/subscribe/')
        found5 = resolve('/trials6/registviews/')
        found6 = resolve('/trials6/validate/')
        self.assertEqual(found.func, index)
        self.assertEqual(found2.func, profile)
        self.assertEqual(found3.func, books)
        self.assertEqual(found4.func, subscribe)
        self.assertEqual(found5.func, registviews)
        self.assertEqual(found6.func, validate)

    def test_story_6_using_index_and_profile_template(self):
        response = Client().get('/trials6/')
        response2 = Client().get('/trials6/profile/')
        response3 = Client().get('/trials6/books/')
        response4 = Client().get('/trials6/subscribe/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertTemplateUsed(response2, 'profile.html')
        self.assertTemplateUsed(response3, 'books.html')
        self.assertTemplateUsed(response4, 'subscribe.html')
        
    
    #checking if the content has "hello apa kabar" in it
    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    #checking if people can put the status in
    def test_model_can_create_new_status(self):
        # Creating new status
        new_status = Statusmodels.objects.create(from_who= 'mahasiswa pusing', statuses='lagi dikerjar deadlines')

        # Retrieving status that has been made
        counting_all_status = Statusmodels.objects.all().count()
        self.assertEqual(counting_all_status, 1)


    def test_story_6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/trials6/', {'from_who': test, 'statuses': test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/trials6/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

     #checking if people can put the status in
    def test_model_can_create_new_user(self):
        # Creating new status
        new_user = Registmodels.objects.create(name= 'mahasiswa pusing', password='bismillah', email='pusing@deadline.com')

        # Retrieving status that has been made
        counting_all_user = Registmodels.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_story_10_post(self):
        name = 'Nama'
        email = 'siapakah@dia.com'
        password = 'bisalulusppw'
        response_post = Client().post('/trials6/subscribe/', {'name': name, 'password': password, 'email': email})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/trials6/subscribe/')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_check_email_already_exist_view_get_return_200(self):
        Registmodels.objects.create(name="bisakok", email="bisakok@gmail.com", password="ppwppw")
        response = Client().post('/trials6/validate/', data={"email": "bisakok@gmail.com"})
        self.assertEqual(response.json()['is_exist'], True)
    
    def test_check_email_hasnt_been_used(self):
        # Registmodels.objects.create(name="capaci", email="capaci@gmail.com", password="capaci")
        response = Client().post('/trials6/validate/', data={"email": "capaci@gmail.com"})
        self.assertEqual(response.json()['is_exist'], False)

    def test_green_alert_shown(self):
        Registmodels.objects.create(name="siapanamanya", email="diadia@gmail.com", password="ppwppw")
        response = Client().post('/trials6/registviews/', data={"name" : "siapanamanya", "password":"ppwppw", "email": "bisakok@gmail.com"})
        self.assertEqual(response.json()['stat'], 'valid')






