from django import forms
# from django.forms import ModelForm
from .models import Statusmodels
from .models import Registmodels
# from django.core.validators import validate_email

class Statusforms(forms.Form):
    error_messages = {
        'required': 'Please fill',
    }

    attributes = {
        'type': 'text',
        'class' : 'form-control',
        'placeholder' : 'namamu'
    }

    attributes_status = {
        'type': 'text',
        'class' : 'form-control',
        'placeholder' : 'tulis status'
    }


    from_who = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=attributes))
    statuses = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=attributes_status))


class Registforms(forms.Form):
    name = forms.CharField(label='Name', required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text', 'placeholder': 'required'}))
    password = forms.CharField(label='Password', required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'password', 'placeholder':'required'}))
    email = forms.EmailField(label='Email', required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'required'}))