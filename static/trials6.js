$(document).ready(function(){
    $("#load_screen").show();
    setTimeout( panggil, 1000);

    $('.accordion').click(function(){
        $(this).next().slideToggle();
    });
    
    function panggil() {
        $("#load_screen").hide()
    }
    var tf = true;
    $("#btnn").click(function(){
        if(tf) {
            $(".navbar").css("background-color", "#E0FFFF");
            $("body").css("background-color", "#D8DDE4");
            $(".namaku").css("color", "#311E89");
            $(".company").css("color", "#06418D");
            $(".img-thumbnail").css("border-color", "#311E89");
            $(".btn").css("background-color", "#E0FFFF");
            tf = false;
        }
        else {
            $(".navbar").css("background-color", "#DED1D3");
            $("body").css("background-color", "#ffffff");
            $(".namaku").css("color", "#A10115");
            $(".company").css("color", "#D72C16");
            $(".img-thumbnail").css("border-color", "#A10115");
            $(".btn").css("background-color", "#eeeeee");
            tf = true;
        }
    });

    var urladdr = '';
    $('.smgt').click(function() {
        urladdr = $(this).attr('id'); //mengambil id dari html untuk dipakai di url di dalam ajax
        $.ajax({
            type: "GET",
            url: urladdr,
            dataType: "json",
            success: function(results) {
                var things = results["items"]
                var baris = ""
                for(var i = 0; i < things.length; i++) {
                    var res = things[i]

                    var Title = res["volumeInfo"]["title"]
                    var Authors = res["volumeInfo"]["authors"]
                    var Publisher = res["volumeInfo"]["publisher"]

                    baris += "<tr><td>" + Title + "</td>"
                    baris += "<td>" + Authors + "</td>"
                    baris += "<td>" + Publisher + "</td>"
                    baris += "<td> <button type='button' class='fav btn btn-default btn-sm'>&#xe006;</button></td></tr>"
                }
                $("#badanTable").html(baris);
            }        
        });
    });  
    
    var hitung = 0;
    $('.fav').click(function() {
        if( $(this).hasClass('clicked') ) {
            $(this).css("background-color", "#ccc");
            $(this).removeClass('clicked');
            $.ajax({
                url: "/trials6/substractcounter/",
                dataType: 'json',
                success: function(result) {
                    var hitung = JSON.parse(result);
                    $('#suka').html(hitung); 
                }        
            });
        }
        else {
            $(this).addClass('clicked');
            $(this).css("background-color", "#eabed9"); 
            $.ajax({
                url: "/trials6/addcounter/",
                dataType: 'json',
                success: function(result) {
                    var hitung = JSON.parse(result);
                    $('#suka').html(hitung);            
                }        
            });
        }
        

    });   

    // var clicks = 0;
    // $(document).on('click', '.fav', function() {
    //     if( $(this).hasClass('clicked') ) {
    //         clicks -=1;
    //         $(this).css("background-color", "#ccc");
    //         $(this).removeClass('clicked');
    //     }
    //     else {
    //         $(this).addClass('clicked');
    //         $(this).css("background-color", "#eabed9");
    //         clicks++;
    //     }
    //     $('#suka').html(clicks);

    // });   

    $('#subs').click(function() {
        location.href = "/trials6/subscribe/"
    })

    var toPrint = "";
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $('#id_email').change(function(){
        // ajax ke views validate email
        $.ajax({
            headers: {"X-CSRFToken": csrftoken,},
            type:"POST",
            url: "/trials6/validate/",
            data: $('#id_email').serialize(),
            dataType: 'json',
            success: function(res) {
                console.log(res);
                if (res.is_exist == true ) {
                    $("input#submit").hide();
                    toPrint += '<div class="alert alert-danger" role="alert" style="text-align: center">The button is missing! Use another email and then it will be back</div>'
                    $('#alertField').html(toPrint)
                }
                else {
                    $("input#submit").show();
                    
                }
            }
        });
    });

    $("#form").submit(function(e) {
        $.ajax({
            type: "POST",
            url: "/trials6/registviews/",
            data: $('form').serialize(),
            success: function(results) {
                var toPrint = ''
                if(results.stat == 'valid') {
                    toPrint += '<div class="alert alert-success" role="alert" style="text-align: center">Thank you for subscribing. You are ready to go!</div>'
                    $('#alertField').html(toPrint)
                    $('#tableSubs').show();
                }                
            }        
        });
        e.preventDefault();
    });


    $.ajax ({
        type: "GET",
        url: "/trials6/userdata/",
        dataType: "json",
        success: function(people) {
            var newUser = "";
            for (var i = 0; i < people.length; i++) {
                var name = people[i].name;
                var email = people[i].email;
                newUser += "<tr id='delete-" + i + "'><td>" + name + "</td>"
                newUser += "<td>" + email + "</td>"
                newUser += "<td> <button type='button'  class='btn btn-light' onClick=hapus('" + email  + "',"+ i +")  >Unsubscribe</button></td></tr>"
            }
            $("#tableBody").append(newUser); 
        }
    });

        

});

function hapus(email, i) {
    $.ajax ({
        type: "GET",
        url: "/trials6/deleteuser/?email=" + email,
        dataType: "json",
        success: function() {
            $("#delete-" + i).remove();
        }
    });
        
}