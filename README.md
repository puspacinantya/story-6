# **Repository story dan challenge ppw**

Perancangan dan Pemrograman Web| Fakultas Ilmu Komputer, Universitas Indonesia, Semester Ganjil 2018/2019

Puspacinantya
1706043821

## Status pipelines
![Pipeline status](https://gitlab.com/puspacinantya/story-6/badges/master/build.svg)

## Test Coverage
![Django Coverage](https://gitlab.com/puspacinantya/story-6/badges/master/coverage.svg)

## Coverage Report
[![pipeline status](https://gitlab.com/puspacinantya/story-6/badges/master/pipeline.svg)](https://gitlab.com/puspacinantya/story-6/commits/master)

## Link Herokuapp
http://story-6-puspa.herokuapp.com/trials6/

